<?php

/**
 * @file
 * This file is currently not loaded, but it provides functions that should
 * add a checkbox to enable Photoswipe on other formatters. This means that
 * theoretically Photoswipe could be used in conjunction with other display
 * formatters (similarly to how Masonry is implemented).
 *
 * The contrib module 'field_formatter_settings' would be required as a
 * dependency.
 */

/**
 * Implements hook_field_formatter_info_alter().
 */
function photoswipe_extras_field_formatter_info_alter(&$info) {
  $field_types = photoswipe_extras_field_types();
  // @todo Options provided by 'normal' formatter would need to be loaded.
  //$default_options = photoswipe_extras_default_options();

  // Set default values for new PhotoSwipe formatter
  foreach ($field_types as $field => $formatters) {
    foreach ($formatters as $formatter) {
      if (!empty($info[$formatter])) {
        $info[$formatter]['settings']['photoswipe'] = NULL;
        //foreach ($default_options as $option => $default_value) {
        //  $info[$formatter]['settings'][$option] = $default_value;
        //}
      }
    }
  }
}

/**
 * Implements hook_field_formatter_settings_form_alter().
 *
 * Hook provided by 'field_formatter_settings' module.
 */
function photoswipe_extras_field_formatter_settings_form_alter(array &$settings_form, array $context) {
  if (photoswipe_extras_formatter_supported($context)) {
    $options = $context['instance']['display'][$context['view_mode']]['settings'];
    dpm($options);
    // Add Photoswipe options to formatter settings form
    $settings_form['photoswipe'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable PhotoSwipe'),
      '#description' => t('Allow items to be opened in a Photoswipe gallery.'),
      '#default_value' => $options['photoswipe'],
    );
    if (TRUE) { // @todo check that photoswipe library exists.
      //photoswipe_extras_add_options_to_form($settings_form, $options);

      // Only show options when Photoswipe is enabled
      //foreach (photoswipe_extras_default_options() as $option => $default_value) {
      //  $settings_form[$option]['#states']['visible']['input.form-checkbox[name$="[photoswipe]"]'] = array('checked' => TRUE);
      //}
    }
    else {
      // Disable Photoswipe as plugin is not installed
      $settings_form['photoswipe']['#disabled'] = TRUE;
      $settings_form['photoswipe']['#description'] = t('This option has been disabled as the JS Photswipe plugin is not installed.');
    }
  }
}

/**
 * Check if a given field formatter is supported.
 *
 * @param $context
 *   The $context array provided by the Field Formatter Settings module. See
 *   field_formatter_settings.api.php for more information.
 *
 * @return
 *   A boolean indicating the supported status.
 */
function photoswipe_extras_formatter_supported($context) {
  $formatter = $context['instance']['display'][$context['view_mode']];

  // Fields in Views aren't supported
  if ($context['instance']['entity_type'] == 'ctools' && $context['instance']['bundle'] == 'ctools') {
    return FALSE;
  }

  // Get supported field types
  $field_types = photoswipe_extras_field_types();

  // Return TRUE for supported formatters with multi-value fields
  $field_type_supported = array_key_exists($context['field']['type'], $field_types);
  $formatter_supported = $field_type_supported && in_array($formatter['type'], $field_types[$context['field']['type']]);
  $multi_value_field = ($context['field']['cardinality'] > 1) || ($context['field']['cardinality'] == FIELD_CARDINALITY_UNLIMITED);
  if ($formatter_supported && $multi_value_field) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/*
 * Get a list of supported field types and their formatters.
 *
 * @return
 *   An associative array where the keys are field types and the values are
 *   arrays of formatter type names.
 */
function photoswipe_extras_field_types() {
  // Core
  $core_fields = array(
    'image' => array(
      'image',
    ),
  );

  // Add contrib modules' field types and formatters
  $contrib_fields = module_invoke_all('photoswipe_extras_field_types');
  $field_types = array_merge_recursive($core_fields, $contrib_fields);

  // Remove duplicate formatters
  foreach ($field_types as &$field_type) {
    $field_type = array_unique($field_type);
  }

  return $field_types;
}

/**
 * Implements hook_preprocess_HOOK() for theme_field().
 */
function photoswipe_extras_preprocess_field(&$variables) {
  $element = $variables['element'];

  // Get field formatter settings
  $options = field_formatter_settings_get_instance_display_settings($element['#entity_type'], $element['#field_name'], $element['#bundle'], $element['#view_mode']);

  // Display field items in a Photoswipe gallery.
  if (isset($options['photoswipe']) && $options['photoswipe']) {
    //photoswipe_load_assets();
    //$variables['classes_array'][] = 'photoswipe-gallery';
  }
}
