; Drush Make
; ----------

; Core version
core = 7.x

; API version
api = 2

; PhotoSwipe libary
; -----------------
libraries[photoswipe][type] = library
libraries[photoswipe][directory_name] = photoswipe
libraries[photoswipe][download][type] = file
libraries[photoswipe][download][url] = https://github.com/dimsemenov/PhotoSwipe/archive/v4.0.5.zip

; Masonry libraries
; -----------------
libraries[masonry][type] = library
libraries[masonry][directory_name] = masonry
libraries[masonry][download][type] = file
libraries[masonry][download][url] = https://npmcdn.com/masonry-layout@3.3.2/dist/masonry.pkgd.min.js
libraries[imagesloaded][type] = library
libraries[imagesloaded][directory_name] = imagesloaded
libraries[imagesloaded][download][type] = file
libraries[imagesloaded][download][url] = http://imagesloaded.desandro.com/imagesloaded.pkgd.min.js
