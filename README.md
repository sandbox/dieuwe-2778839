# PhotoSwipe Extras

## Setup

Download and install PhotoSwipe, jQuery Update, and the libraries module as
prerequisites.

The PhotoSwipe JavaScript library is also required for the module
to function. If you installed the module via Drush, this library would
have been enabled automatically.

Your site's jQuery version needs to be set to 1.7 or higher, which
will automatically be configured by the base PhotoSwipe module if your
site is not already using a high enough version.

## Example Usage

Navigate to the "Manage fields" tab on a content type of your
choosing, e.g. Structure => Content types => Page => Manage fields,
and add a new image field.

When configuring the field, make sure that the cardinality (number of
values) is set to a number greater than one, and likely to "unlimited".

Navigate to the "Manage display" tab of the content type. Ensure that
the image field is visible on the view mode(s) you would like it show
up on. In the "Format" column, select "Photoswipe" as the formatter
and configure its settings to your liking.

Finally, create a new node, or edit an existing one, and add a number
of images. When you view the node, you should be be to click on one of
the images and PhotoSwipe should open.

### SimplyTestMe

To try out PhotoSwipe for yourself, you can use this URL to set up
with a site with all the modules that provide extra functionality
included:

https://simplytest.me/project/2778839/7.x?add[]=masonry&add[]=masonry_fields&add[]=field_formatter_settings&add[]=ctools&add[]=views&add[]=file_entity&add[]=media&add[]=oembed

Once the site has finished building, navigate to the modules page and
enable Media Internet Sources and oEmbed for video support, or Masonry
Fields for Masonry support.

## Masonry Integration

If you install and enable Masonry API (masonry), Masonry Fields
(masonry_fields), and their prerequisites then you should be able to
configure Masonry settings on the Photoswipe formatter.

## Video Support

Currently, video support is limited to YouTube via the Media Internet
Sources and oEmbed modules.

Install and enable Media, oEmbed, and
their prerequisites. Media Internet Sources, a media submodule, also
needs to be enabled.

Configure your image field to use the "Media browser" widget and make
sure that both image and video file types are allowed.

To add a video, go back to your node, click "attach media", and
navigate to the "web" tab. Paste a YouTube URL into the field. Save
your node and click on the thumbnail to play the video in PhotoSwipe.
