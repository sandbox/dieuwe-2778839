/**
 * @file
 */
/**
 * PhotoSwipeGallery
 *
 * Opens a PhotoSwipe Modal popup based on data attributes of a gallery.
 *
 */
(function($) {

  /* Helper functions */
  var closest = function closest(el, fn) {
    return el && ( fn(el) ? el : closest(el.parentNode, fn) );
  };

  var youtube_reg = (/youtube.com\/watch\?v=(.*)/i);

  /* Main wrapper class*/
  var PhotoSwipeGallery = function (selector, options) {
    this.selector = selector;
    this.options = options || {};
    this.photoswipeModal = null;
  };

  PhotoSwipeGallery.prototype.parseThumbnailElements = function (el) {
    var thumbElements = el.querySelectorAll('a'),
        numNodes = thumbElements.length,
        items = [],
        childElements,
        thumbnailEl,
        matches,
        el,
        size,
        href,
        item;

    for(var i = 0; i < numNodes; i++) {
      el = thumbElements[i];

      // include only element nodes
      if(el.nodeType !== 1) {
        continue;
      }

      childElements = el.children;

      href = el.getAttribute('href');
      matches = youtube_reg.exec(href);

      item = {};

      if(matches && matches.length>1) {
        youtube_id = matches[1];
        item.w = 640;
        item.h = 480;
        item.src = "https://img.youtube.com/vi/" + youtube_id + "/sddefault.jpg";
        item.youtube_id = youtube_id;
        item.youtube_src = href;
      } else {
        size = el.getAttribute('data-size').split('x');
        item.w = parseInt(size[0], 10);
        item.h = parseInt(size[1], 10);
        item.src = href;
      }

      item.el = el; // save link to element for getThumbBoundsFn

      if(childElements.length > 0) {
        item.msrc = childElements[0].getAttribute('src'); // thumbnail url
        if(childElements.length > 1) {
            item.title = childElements[1].innerHTML; // caption (contents of figure)
        }
      }

      var mediumSrc = el.getAttribute('data-med');
      if(mediumSrc) {
        size = el.getAttribute('data-med-size').split('x');
        // "medium-sized" image
        item.m = {
            src: mediumSrc,
            w: parseInt(size[0], 10),
            h: parseInt(size[1], 10)
        };
      }

      // original image
      item.o = {
        src: item.src,
        w: item.w,
        h: item.h
      };

      items.push(item);
    }

    this.gallery_items = items;
    return items;
  }

  PhotoSwipeGallery.prototype.setupGallery = function () {
    // parse gallery object and extract data properties
    var galleryElements = this.galleryElements = document.querySelectorAll( this.selector );
    var self = this;

    for(var i = 0, l = galleryElements.length; i < l; i++) {
      galleryElements[i].setAttribute('data-pswp-uid', i+1);
      galleryElements[i].onclick = function (e) {
        self.onThumbnailsClick(e);
      }
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    if( !this.options.disable_hash) {
      var hashData = this.parseHash();
      if(hashData.pid && hashData.gid) {
        this.openPhotoSwipe( hashData.pid,  galleryElements[ hashData.gid - 1 ], true, true );
      }
    }
  };

  PhotoSwipeGallery.prototype.openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        options,
        items;

    items = this.parseThumbnailElements(galleryElement);

    // define options (if needed)
    options = {
      galleryUID: galleryElement.getAttribute('data-pswp-uid'),
      getThumbBoundsFn: function(index) {
        var thumbnail = items[index].el.children[0],
            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
            rect = thumbnail.getBoundingClientRect();
        return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
      },
      addCaptionHTMLFn: function(item, captionEl, isFake) {
        if(!item.title) {
          captionEl.children[0].innerText = '';
          return false;
        }
        captionEl.children[0].innerHTML = item.title;
        return true;
      }
    };

    if(fromURL) {
      if(options.galleryPIDs) {
        // parse real index when custom PIDs are used
        // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
        for(var j = 0; j < items.length; j++) {
          if(items[j].pid == index) {
            options.index = j;
            break;
          }
        }
      } else {
        options.index = parseInt(index, 10) - 1;
      }
    } else {
      options.index = parseInt(index, 10);
    }

    // exit if index not found
    if( isNaN(options.index) ) {
      return;
    }

    if (this.options.minimal) {
      options.mainClass = 'pswp--minimal--dark';
      options.barsSize = {top:0,bottom:0};
      options.captionEl = false;
      options.fullscreenEl = false;
      options.shareEl = false;
      options.bgOpacity = 0.85;
      options.tapToClose = true;
      options.tapToToggleControls = false;
    }

    if(disableAnimation) {
        options.showAnimationDuration = 0;
    }

    // Pass data to PhotoSwipe and initialize it, using our custom UI.
    var gallery = this.photoswipeModal = new PhotoSwipe(pswpElement, PhotoSwipeUI_Extras, items, options);

    // see: http://photoswipe.com/documentation/responsive-images.html
    var realViewportWidth,
        useLargeImages = false,
        firstResize = true,
        imageSrcWillChange;

    gallery.listen('beforeResize', function() {
      var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
      dpiRatio = Math.min(dpiRatio, 2.5);
      realViewportWidth = gallery.viewportSize.x * dpiRatio;

      if(realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200 ) {
        if(!useLargeImages) {
          useLargeImages = true;
          imageSrcWillChange = true;
        }
      } else {
        if(useLargeImages) {
          useLargeImages = false;
          imageSrcWillChange = true;
        }
      }

      if(imageSrcWillChange && !firstResize) {
        gallery.invalidateCurrItems();
      }

      if(firstResize) {
        firstResize = false;
      }

      imageSrcWillChange = false;
    });

    gallery.listen('gettingData', function(index, item) {
      if( useLargeImages ) {
        item.src = item.o.src;
        item.w = item.o.w;
        item.h = item.o.h;
      } else {
        // Fix: originally instead of "o" was using "m" (possibly to do with
        // bandwidth? For some reason that object doesn't exist though.
        item.src = item.o.src;
        item.w = item.o.w;
        item.h = item.o.h;
      }
    });

    var self = this;
    gallery.listen('afterChange', function() {
      self.detectVideo(gallery);
    });

    gallery.listen('beforeChange', function() {
      self.removeVideo();
    });

    gallery.listen('close', function() {
      self.removeVideo();
    });

    gallery.init();
  }

  PhotoSwipeGallery.prototype.detectVideo = function () {
    var item = this.photoswipeModal.currItem;
    var youtube = item.youtube_id;
    if (youtube) {
        this.addVideo(item);
    }
  };

  PhotoSwipeGallery.prototype.addVideo = function (item, vp) {
    var v = $('<div />', {
                class:'pswp__video',
                css : {
                  'width':item.w,
                  'height':item.h
                }
    });
    v.append('<div class="pswp__video-play">');

    v.one('click touchstart', (function() {
        var player_html = '<iframe class="ps-video-container__youtube" ' +
                          'width="' + item.w + '" height="' + item.h +
                          '" src="https://www.youtube.com/embed/' + item.youtube_id + '?rel=0&autoplay=1" ' +
                          'frameborder="0" allowfullscreen></iframe>';
        v.html(player_html);
        $('.pswp__img').css('visibility','hidden');
    }));

    v.appendTo('.pswp__scroll-wrap'); // change this to be scoped to the current gallery
  };

  PhotoSwipeGallery.prototype.removeVideo = function() {
    if ($('.pswp__video').length > 0) {
      $('.pswp__img').css('visibility','visible');
      $('.pswp__video').remove();
    }
  };

  PhotoSwipeGallery.prototype.onThumbnailsClick = function (e) {
    e = e || window.event;
    e.preventDefault ? e.preventDefault() : e.returnValue = false;

    var eTarget = e.target || e.srcElement;

    var clickedListItem = closest(eTarget, function(el) {
        return el.tagName === 'A';
    });

    if(!clickedListItem) {
        return;
    }

    var clickedGallery = this.galleryElement = $(clickedListItem).closest('.photoswipe-gallery')[0];

    var childNodes = clickedGallery.querySelectorAll('a'),
        numChildNodes = childNodes.length,
        nodeIndex = 0,
        index;

    for (var i = 0; i < numChildNodes; i++) {
        if(childNodes[i].nodeType !== 1) {
            continue;
        }

        if(childNodes[i] === clickedListItem) {
            index = nodeIndex;
            break;
        }
        nodeIndex++;
    }

    if(index >= 0) {
        this.openPhotoSwipe( index, clickedGallery );
    }

    return false;
  };

  PhotoSwipeGallery.prototype.parseHash = function() {
    var hash = window.location.hash.substring(1),
    params = {};

    if(hash.length < 5) { // pid=1
        return params;
    }

    var vars = hash.split('&');
    for (var i = 0; i < vars.length; i++) {
        if(!vars[i]) {
            continue;
        }
        var pair = vars[i].split('=');
        if(pair.length < 2) {
            continue;
        }
        params[pair[0]] = pair[1];
    }

    if(params.gid) {
      params.gid = parseInt(params.gid, 10);
    }

    return params;
  };

  Drupal.behaviors.photoswipeExtras = {
    attach: function (context) {
      var psGallery = new PhotoSwipeGallery('.photoswipe-gallery');
      psGallery.setupGallery();
    }
  }
})(jQuery);
